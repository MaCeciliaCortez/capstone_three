@extends('layout.app')
@section('content')

<div class="container-fluid px-2 my-2">
    <div class="row"> <!-- start card -->
        <div class="col-md-4 col-12">
                <div class="cardshadow p-3 mb-5 bg-info rounded">
                        <img src="{{ asset('images/bg.jpg') }}" class="img-fluid card-img-top">
                        <div class="card-body">
                            <h5>Math Class</h5>
                                <p class="card-text text-justify">A class where kids get to play with numbers. Fun math games and activities are done to make math more fun.</p>
                                <a id="seebut" class="btn bg-light" data-toggle="modal" data-target="#newBooking">Book Class</a>
                        </div> <!-- end card body -->
                 </div>
        </div>

        <div class="col-md-4 col-12">
            <div class="cardshadow p-3 mb-5 bg-warning rounded">
                    <img src="{{ asset('images/bg.jpg') }}" class="img-fluid card-img-top">
                    <div class="card-body">
                        <h5>Art Class</h5>
                            <p class="card-text text-justify">A class where kids get to play with numbers. Fun math games and activities are done to make math more fun.</p>
                            <a id="seebut" class="btn bg-light" data-toggle="modal" data-target="#newBooking">Book Class</a>
                    </div> <!-- end card body -->
            </div>
        </div>
        <div class="col-md-4 col-12">
                <div class="cardshadow p-3 mb-5 bg-success rounded">
                        <img src="{{ asset('images/bg.jpg') }}" class="img-fluid card-img-top">
                        <div class="card-body">
                            <h5>Dance Class</h5>
                                <p class="card-text text-justify">A class where kids get to play with numbers. Fun math games and activities are done to make math more fun.</p>
                                <a id="seebut" class="btn bg-light" data-toggle="modal" data-target="#newBooking">Book Class</a>
                        </div> <!-- end card body -->
                </div>
        </div>
        <div class="col-md-4 col-12">
                <div class="cardshadow p-3 mb-5 bg-warning rounded">
                        <img src="{{ asset('images/bg.jpg') }}" class="img-fluid card-img-top">
                        <div class="card-body">
                            <h5>Science Class</h5>
                                <p class="card-text text-justify">A class where kids get to play with numbers. Fun math games and activities are done to make math more fun.</p>
                                <a id="seebut" class="btn bg-light" data-toggle="modal" data-target="#newBooking">Book Class</a>
                        </div> <!-- end card body -->
                 </div>
        </div>

        <div class="col-md-4 col-12">
            <div class="cardshadow p-3 mb-5 bg-success rounded">
                    <img src="{{ asset('images/bg.jpg') }}" class="img-fluid card-img-top">
                    <div class="card-body">
                        <h5>Karate Class</h5>
                            <p class="card-text text-justify">A class where kids get to play with numbers. Fun math games and activities are done to make math more fun.</p>
                            <a id="seebut" class="btn bg-light" data-toggle="modal" data-target="#newBooking">Book Class</a>
                    </div> <!-- end card body -->
            </div>
        </div>
        <div class="col-md-4 col-12">
                <div class="cardshadow p-3 mb-5 bg-info rounded">
                        <img src="{{ asset('images/bg.jpg') }}" class="img-fluid card-img-top">
                        <div class="card-body">
                            <h5>Language Class</h5>
                                <p class="card-text text-justify">A class where kids get to play with numbers. Fun math games and activities are done to make math more fun.</p>
                                <a id="seebut" class="btn bg-light" data-toggle="modal" data-target="#newBooking">Book Class</a>
                        </div> <!-- end card body -->
                </div>
        </div>
    </div>
</div>

{{-- //CREATE A BOOKING --}}
                {{-- new booking modal --}}
                <div class="modal" id="newBooking">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title">Book A Class</h4>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div> {{-- end modal header --}}
                                <div class="modal-body">
                                    <form id="createBooking">
                                        @csrf

                                        <div class="form-group">
                                            <label for="name">Name Of Child: </label>
                                            <input type="text" id="name" class="form-control" name="name">
                                        </div>

                                        <div class="form-group">
                                            <label for="class">Class: </label>
                                            <input type="text" id="class" class="form-control" name="class">
                                        </div>

                                        {{-- <div class="form-group">
                                            <label for="class-category">Class:</label>
                                            <select name="category" id="class-category" class="form-control">
                                                @foreach($categories as $category)
                                                    <option value="{{ $category->id }}"> {{ $category->name }}</option>
                                                @endforeach
                                            </select>
                                        </div> --}}

                                       <div class="form-group">
                                            <label for="batch">Start Date: </label>
                                            <input type="date" id="start" class="form-control" name="start">
                                        </div>

                                        <div class="form-group">
                                            <label for="batch">End Date: </label>
                                            <input type="date" id="end" class="form-control" name="end">
                                        </div>

                                        <button id="createButton" class="btn btn-primary btn-block" data-dismiss="modal"> Create Booking</button>
                                    </form>
                                </div>


                            </div>
                        </div>
                    </div> {{-- new dev modal --}}

                    <script>
                        // create a new booking
                        document.querySelector("#createButton").addEventListener("click", function() {
                            // alert("hello")
                            let nameOfChild = document.querySelector("#name")
                            let start = document.querySelector("#start")
                            let end = document.querySelector("#end")
                            // console.log(nameField)
                            // console.log(batch)
                            // console.log(portfolio)

                            let formData = new FormData()

                            formData.name = nameOfChild.value
                            formData.start = start.value
                            formData.end = end.value

                            // console.log(formData)
                            fetch("http://localhost:3000/booking/create", {
                                method: "POST",
                                headers : {
                                    'Content-Type' : 'application/json'
                                },
                                body : JSON.stringify(formData)
                            })
                            .then(res=>res.json())
                            .then(res => {
                                console.log(res.name)
                                //add the class alert-success to #success
                                document.querySelector("#success").classList.add("alert-success")
                                //change the content of #success to print out "Successfully created the booking (name of child)"
                                document.querySelector("#success").innerHTML = "Successfully created the booking " + res.nameOfChild
                            })
                            .catch(error => console.error('Error:', error))

                        })
                    </script>



        <h1>Daily Schedule</h1>
        <p>8:30-8:45 AM</p>
        <p>TEACHERS PREPARE FOR SCHOOL</p>
        <p>Preparation - The staff prepare the classroom by taking down chairs from the table, placing rug pieces on the floor for Circle Time, preparing the snack for the day, setting up art projects and developmental skill areas on the tables, turning on the computer, preparing sanitizing solution and washing hands for the day.
        </p>
        <p>8:45 – 9:00 AM
        <p>CHILDREN’S ARRIVAL</p>
        <p>Arrival - Families are welcomed to the preschool. The teachers greet the children and encourage them to hang up their coat and backpack in their locker and then help them transition to a free play activity. We suggest that parents or caregivers only stay a few minutes to say goodbye. Families are encouraged to have their child arrive by 9:00 AM.
        </p>
        <p>9:00 – 9:25 AM</p>
        <p>FREE PLAY</p>
        <p>Free Play – Free play provides opportunities for learning and social interaction. Self-directed centers of interest are set up to allow children to independently choose activities that help them separate and adjust to the beginning of the day.
        </p>
        <p>9:25 – 9:40</p>
        <p>CIRCLE TIME</p>
        <p>Circle Time – Circle Time is a more structured setting where children are asked to practice self-regulation, increase attention span, and develop listening skills. The children greet one another, record the weather, mark the calendar, sing songs, listen to stories, practice finger plays, and talk about what they would like to learn that day. We encourage children to take turns talking and listening to each other.
        </p>
        <p>9:40 – 10:50 AM</p>
        <p>ACTIVITY CENTER TIME</p>
        <p>Activity Center Descriptions – Children are encouraged to freely choose any and all centers listed below. Specific skills checks and activities are facilitated by the teachers during Activity Center Time.
        </p>
        <p>10:50 – 11:10 AM</p>
        <p>CLEAN-UP/SNACK/HEALTH</p>
        <p>Clean Up/Snack/Health – Children share the responsibility of cleaning up the preschool. Self-help skills such as independent toileting, washing hands, pouring juice, and rinsing cups are encouraged. Snack is also a social time where children learn table manners and visit with each other and the teachers. Healthy eating is promoted and a variety of nutritional foods are a part of snack-time.
        </p>
        <p>11:10 – 11:35 AM</p>
        <p>OUTSIDE PLAY, CREATIVE MOVEMENT, SHOW & TELL OR MUSIC TIME</p>
        <p>Outside Play & Creative Movement – We use the playground outside the preschool whenever possible. Children develop their large motor and social skills during outside play. A climbing structure, swings, sliding board, tricycles, balls, scooters and a child sized playhouse are included in the playground equipment. The M/W/F class walks to the college’s Old ’77 gym on Wednesdays for creative movement sessions during the winter months.
        </p>
        <p>11:40 AM</p>
        <p>DISMISSAL</p>
        <p>Dismissal – Departure is the closing routine of a busy day. The children sing the “Good-bye Song”, practice self-help skills by putting on their coats, and gathering projects or information to be taken home. The parent or caregiver is asked to enter the classroom to pick-up their child.
        </p>
    </div>









@endsection
