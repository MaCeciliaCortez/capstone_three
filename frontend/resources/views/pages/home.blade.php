@extends('layout.app')
@section('content')

<article id="article_left">
        
                    {{-- //LOGIN AND REGISTER --}}
                    <div id="login">
                        <div class="container">
                            <div id="login-row" class="row justify-content-left align-items-left">
                                <div id="login-column">
                                    <div id="login-box">
                                        <form id="login-form" class="form">
                                            @csrf
                                            <h4 class="text-dark">Login to your account</h4>

                                            <div class="form-group">
                                                <label for="email" class="text-dark">Email Address:</label><br>
                                                <input type="text" name="email" id="email" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label for="password" class="text-dark">Password:</label><br>
                                                <input type="password" name="password" id="password" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <button class="btn btn-primary" id="logIn">Log In</button>
                                            </div>
                                            <a href="#" class="text-right text-danger italic">Not yet registered? Click here.</a>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    Bookings
                    <div id="bookings"></div>
                </article>

            <article id="article_right">
                <h3 id="headline" class="py-2"></h3>
                <p id="p1"></p>

                <h3>Address</h3>
                <p>Enzo Bldg, Jupiter Street<br/>Poblacion, Makati City 1234</p>

                <h3>Enroll your child now!
                    <button id="add-booking" class="btn btn-info" data-toggle="modal" data-target="#newBooking">Book A Class</button>

                </h3>

                {{-- //CREATE A BOOKING --}}
                {{-- new booking modal --}}
                <div class="modal" id="newBooking">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Book A Class</h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div> {{-- end modal header --}}
                            <div class="modal-body">
                                <form id="createBooking">
                                    @csrf

                                    <div class="form-group">
                                        <label for="name">Name Of Child: </label>
                                        <input type="text" id="name" class="form-control" name="name">
                                    </div>
                                    <div class="form-group">
                                        <label for="name">Class: </label>
                                        <input type="text" id="className" class="form-control" name="className">
                                    </div>

                                    {{-- <div class="form-group">
                                        <label for="class-category">Class:</label>
                                        <select name="category" id="class-category" class="form-control">
                                                <option value="{{ $category->id }}"> {{ $category->name }}</option>
                                        </select>
                                    </div> --}}

                                   <div class="form-group">
                                        <label for="batch">Start Date: </label>
                                        <input type="date" id="start" class="form-control" name="start">
                                    </div>

                                    <div class="form-group">
                                        <label for="batch">End Date: </label>
                                        <input type="date" id="end" class="form-control" name="end">
                                    </div>

                                    <button id="createButton" class="btn btn-primary btn-block" data-dismiss="modal"> Create Booking</button>
                                </form>
                            </div>


                        </div>
                    </div>
                </div> {{-- new dev modal --}}


                <p>
                    <a href="https://www.facebook.com/" target="cloud"><img src="https://www.seeklogo.net/wp-content/uploads/2013/11/facebook-flat-vector-logo-400x400.png" width="30" border="0" alt="Facebook"/></a>
                    <a href="https://www.instagram.com" target="cloud"><img src="https://workingwithdog.com/wp-content/uploads/2016/05/new_instagram_logo-1024x1024.jpg" width="30" border"0" alt="instgram"/></a>
                    <a href="https://www.twitter.com" target="cloud"><img src="http://www.clipartbest.com/cliparts/eTM/pzr/eTMpzr7Ac.png" width="30" border="0" alt="twitter"></a>
                </p>
            </article>
        <script src="{{ asset('js/login.js') }}" defer></script>
        <script src="{{ asset('js/booking.js') }}" defer></script>
@endsection
