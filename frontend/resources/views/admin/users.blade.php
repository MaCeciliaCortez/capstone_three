@extends('layout.adminui')
@section('content')
<div class="container">
        @if(Session::has('success_message'))
            <div class="alert alert-success">
                {{ Session::get('success_message') }}
            </div>
        @endif
        @if(Session::has("error_message"))
            <div class="alert alert-danger">
                {{Session::get("error_message")}}
            </div>
        @endif
        <h1 class="page-title"><span class="base">{{$user->name}}</span></h1>
        <div class="row">
			<div class="col-md-8 mx-auto">
				<form action="/admin/users/update{{$user->id}}" method="POST" enctype="multipart/form-data">
					@csrf
					{{ method_field("PATCH") }}
					<div class="form-group">
						<label for="name">Name:</label>
						<input type="text" name="name" id="name" class="form-control" value="{{$user->name}}" required>
					</div>

					<div class="form-group">
						<label for="email">Email:</label>
						<input type="text" name="email" id="email" class="form-control" value="{{$user->email}}" required>
					</div>

					<div class="form-group">
						<label for="price">Role:</label>
                        <select name="role" id="role" class="form-control">
                            @php $roles = array('user', 'admin'); @endphp

							@foreach($roles as $index => $role)
								<option value="{{$role}}"
									{{$role == $user->role ? "selected" : ""}}>
										{{ucfirst($role)}}
								</option>
							@endforeach
						</select>
					</div>

					<button class="btn btn-success" type="submit">Update</button>
				</form>
			</div>
		</div>
    </div>
@endsection
