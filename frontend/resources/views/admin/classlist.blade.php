@extends('layout.adminui')
@section('content')

<div class="container">
        @if(Session::has('success_message'))
            <div class="alert alert-success">
                {{ Session::get('success_message') }}
            </div>
        @endif
        @if(Session::has("error_message"))
            <div class="alert alert-danger">
                {{Session::get("error_message")}}
            </div>
        @endif
        <h1 class="page-title"><span class="base">Class List</span></h1>
        <div class="row">
            <div class="col">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col">Name</th>
                            <th scope="col">Description</th>
                            <th scope="col">Created At</th>
                            <th scope="col">Action</th>

                        </tr>
                    </thead>
                    {{-- @foreach($klases as $klase)
                        <tbody>
                            <td>{{$klase->name}}</td>
                            <td>{{$klase->description}}</td>
                            <td>{{$klase->created_at}}</td>
                            <td>
                            <td>
                        </tbody>
                    @endforeach --}}
                    <div class="row">
                            <form id="edit-class">
                                @csrf
                                <button type="submit" class="btn btn-primary"><span>Edit</span></button>
                            </form>
                            <button class="btn btn-danger mx-1">Delete</a>
                        </div>
                    </td>
                </table>
            </div>
        </div>
    </div>
    <script src="{{ asset('js/class.js') }}" defer></script>
@endsection
