@extends('layout.adminui')
@section('content')

@extends('layout.userui')
@section('content')

<div class="container">
		@if(Session::has("success_message"))
			<div class="alert alert-success">
				{{Session::get("success_message")}}
			</div>
		@endif
		@if(Session::has("error_message"))
			<div class="alert alert-danger">
				{{Session::get("error_message")}}
			</div>
		@endif
        <h1 class="page-title"><span class="base">Booking Transactions</span></h1>


		<div class="row">
			<div class="col">
				<table class="table table-hover">
					<thead>
						<tr>
							<th scope="col">Name Of Child</th>
							<th scope="col">Class</th>
							<th scope="col">Start Date</th>
							<th scope="col">End Date</th>
						</tr>
					</thead>
					@foreach($bookings as $booking)
						<tbody>
							<td>{{$booking->id}}</td>
							<td>{{$booking->created_at}}</td>
							<td>
								@foreach($users as $user)
									@if($booking->user_id == $user->id)
										{{$user->name}}
									@endif
								@endforeach
							</td>
							<td class="booking_status" data-id="{{$booking->id}}">{{ucfirst($booking->status->name)}}</td>

							<td>
								<div class="row">
									@if(Auth::user()->role=="admin")
										<form method="GET" id="selected_status">
											@csrf
											<select class="status_select" name="status" id="status" >
												@foreach(\App\Status::all() as $status)
													{{-- {{ $status }} --}}
													@php
														$selected = ($booking->status_id == $status->id) ? 'selected="selected"' : '';
													@endphp
													<option value="{{$status->id}}" {{$selected}}>{{ucfirst($status->name)}}</option>
												@endforeach
											</select>
										</form>
									@else
										<form action="/admin/bookings/cancel/{{$booking->id}}" method="get" id="cancel-booking">
											@csrf
											@php
												$canceled = ($booking->status_id == 3) ? 'disabled="disabled"' : '';
												$btnClass = ($booking->status_id == 3) ? 'btn-secondary' : 'btn-danger'
											@endphp
											<button type="submit" class="btn {{$btnClass}}" {{$canceled}}><span>Cancel Booking</span></button>
										</form>
									@endif
									<input type="hidden" name="order_id" value="{{$booking->id}}" />
								</div>
							</td>
							<td>
						</tbody>
					@endforeach
				</table>
			</div>
		</div>
	</div>

@endsection



@endsection
