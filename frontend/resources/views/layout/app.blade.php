<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>ABBAS HOUSE DAYCARE</title>

  <!-- Scripts -->
  <script src="{{ asset('js/jquery/jquery-3.4.1.min.js') }}"></script>
  <script src="{{ asset('js/app.js') }}" defer></script>

  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">

  <!-- Fonts -->
  <link rel="dns-prefetch" href="//fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css?family=Gloria+Hallelujah|Indie+Flower|Playfair+Display|Shadows+Into+Light&display=swap" rel="stylesheet">

  <!-- Styles -->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">

</head>
<body>

    <div id="container">

      <nav>
        <a href="./home">The Abbas |</a>
        <a href="./classes">Our Classes |</a>
        <a href="./gallery">Gallery |</a>
        <a href="./contact">Contact</a></td>
        <button id="logout-but" class="btn btn-danger">Logout</button>
      </nav>
     

        <section>
                <iframe width="850" height="250" src="https://www.youtube.com/embed/I0fANGONKq8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            {{-- <img src="https://i.cbc.ca/1.3443007.1496927837!/fileImage/httpImage/image.jpg_gen/derivatives/16x9_780/daycare.jpg" height="250" alt="" /> --}}
        </section>

        <main>
            @yield('content')
        </main>

        

    <footer class="footer text-center">
        <p>Cecilia Cortez |

        <a href="https://www.facebook.com" target="blank"> <i class="fab fa-facebook-f"></i></a>

        <a href="https://gitlab.com" target="blank"><i class="fab fa-gitlab"></i></a>


        <a href="https://instagram.com" target="blank"><i class="fab fa-instagram"></i></a>

        | Tuitt Bootcamp
        </p>
    </footer>

</div>
<script src="{{ asset('js/logout.js') }}" defer></script>
</body>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>
</html>
