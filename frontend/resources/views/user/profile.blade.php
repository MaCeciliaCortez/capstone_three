@extends('layout.userui')
@section('content')

    <div id="success-message"><div>
        <h1 class="page-title text-center">Hello</h1>
        <div class="row">
			<div class="col-md-8 mx-auto">
				<form>
					@csrf
					{{ method_field("PATCH") }}
					<div class="form-group">
						<label for="name">Email:</label>
						<input type="text" name="email" id="email" class="form-control"  required>
                    </div>
                    

					<button class="btn btn-success" id="update" type="submit">Update</button>
				</form>
			</div>
		</div>
    </div>

    <div id="user-container">
    </div>

    <script src="{{ asset('js/user.js') }}" defer></script>
@endsection
