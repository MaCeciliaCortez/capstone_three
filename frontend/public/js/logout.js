document.querySelector("#logout-but").addEventListener("click", function() {
    // alert('hello')
    if(window.confirm("Are you sure you want to logout?")) {
        // alert("hello")
        fetch('http://localhost:3000/auth/logout')
        .then((res) => {
            return res.json();
        })
        .then((data) => {
            console.log(data)
            localStorage.clear()
            window.location.replace('/');
        })
        .catch(function(err) {
            console.log(err)
        })
    }
    console.log(localStorage.user)
    console.log(localStorage.isAdmin)
    console.log(localStorage.token)
})