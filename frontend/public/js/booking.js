const token = localStorage.token;

//create button
document.querySelector("#createButton").addEventListener("click", function(e) {
  e.preventDefault()

  let name = document.querySelector("#name").value
  let className = document.querySelector("#className").value
  let start = document.querySelector("#start").value
  let end = document.querySelector("#end").value

  const form = {
    nameOfChild: name,
    class: className,
    startDate: start,
    endDate: end
  }

  fetch("http://localhost:3000/booking/create", {
      method: "POST",
      headers : {
          'Accept': 'application/json',
          'Content-Type' : 'application/json',
          'Authorization':`Bearer ${token}`
      },
      body : JSON.stringify(form)
  }).then((res) => {
    console.log(res, 'RES')
  })
  
  console.log(form, 'TEST')
})

//openEditModal
document.querySelector("#editModal").addEventListener("click", function(e, id) {
  e.preventDefault()

  let name, start, end

  fetch("http://localhost:3000/booking/create", {
      method: "POST",
      headers : {
          'Content-Type' : 'application/json',
          'Authorization':`Bearer ${token}`
      },
      body : form
  }).then((res) => {
    console.log(res, 'RES')
  })

  const form = {
    name: name,
    start: start,
    end: end
  }

  fetch("http://localhost:3000/booking/create", {
      method: "GET",
      headers : {
          'Content-Type' : 'application/json',
          'Authorization':`Bearer ${token}`
      },
      body : id
  }).then((res) => {
    console.log(res, 'RES')
    document.querySelector("#name").value = res.data.name
    document.querySelector("#start").value = res.data.start
    document.querySelector("#end").value = res.data.end
  })
  
  console.log(form, 'TEST')
})
