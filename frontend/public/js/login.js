		//create a script that when i click on the log in button, an alert with a 'hello' message will pop up
		document.querySelector("#logIn").addEventListener("click", function(e) {
			e.preventDefault()
			// alert('hello')
			//create a variable email that will store the value inputted in #email
			let email = document.querySelector("#email").value

			//create a variable pass that will store the value inputted in #password
			let pass = document.querySelector("#password").value

			// log in the console email and pass
			console.log('The email is: ' + email)
			console.log('The password is: ' + pass)

			// create a fetch request to the backend controller
			fetch('http://localhost:3000/auth/login', {
				method : "POST",
				headers : {
					'Content-Type' : 'application/json'
				},
				body : JSON.stringify({
					email : email,
					password : pass
				})
			})
			.then(res => res.json())
			.then(res => {
				console.log(res)
				//log in the console the JWT
				console.log('The JWT is : ' + res.data.token)
				// log in the console the email of the logged in user
				console.log('The logged in email is: ' + res.data.user.email)
				// log in the console the if the user logged in is an admin
				console.log('Is the user an admin? ' + res.data.user.admin)

				//read up on localStorage
				//read up on its setItem method
				localStorage.setItem('token', res.data.token)
				//store an item in our localStorage with a key user with a value of the logged in user's email property
				localStorage.setItem('user', res.data.user.email)
				//store an item in our localStorage with a key isAdmin with a value of the logged in user's admin property
				localStorage.setItem('isAdmin', res.data.user.admin)
				localStorage.setItem('id', res.data.user._id)
				console.log(res.data.user._id);

				window.location = '/user/profile'

			})
			.catch(error => console.error('Error: ', error))

		})