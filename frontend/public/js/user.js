// alert('hello');//

// retrieve devs from db
            // document.querySelector("#devList").innerHTML = "Hello world"
            //create a token variable that will store the value of the key token in the localStorage
            let token = "Bearer "+localStorage.token

            //create an email variable that will store the value of the key user in the localStorage
            let email = localStorage.user

            //create an isAdmin variable that will store the value of the key isAdmin in the localStorage
            let isAdmin = localStorage.isAdmin

            // console.log('the token from lS is: ' + token)

            const id = localStorage.getItem("id")

            console.log(id)

            const url = `http://localhost:3000/user/${id}`
            fetch(url, {
                method : 'GET',
                headers : {
                    'Content-Type'  : 'application/json',
                    'Authorization' : token
                }
            })
            .then(res=> {
                return res.json()
            })
            .then(data => {
                // console.log(data.user)
                const user = data.user
                // let userGroups = ' ';

                // users.map(user => {
                //     // console.log(user)
                //     userGroups += `
                //         <li class="card p-5 text-center"> ${user.name}
                //             <button class="deleteButton" data-id="${user._id}">Delete
                //             </button>
                //            <button class="editButton" data-id="${user._id}" data-toggle="modal" data-target="#editModal">Edit
                //             </button>
                //         </li>
                //     `
                // })
                // document.querySelector("#user-container").innerHTML = userGroups;
                document.querySelector("#email").setAttribute("value",user.email);

            })
        
            document.querySelector("#update").addEventListener("click",function(event) {
                
                const url = `http://localhost:3000/user/${id}`
                console.log(url);

                //event.preventDefault();

                let email = document.querySelector("#email").value;
                let formData = new FormData();

                formData.email = email;

                fetch(url, {
                    method : 'PUT',
                    headers : {
                        'Content-Type'  : 'application/json',
                        'Authorization' : token
                    },
                    body : JSON.stringify(formData)
                })
                .then(res => {
                    window.location ='/user/profile';
                })
                .then(data => {
                    console.log(data)
                    const message = data.message;
                    document.querySelector("#success-message").innerHTML = message;
                     //instantiate a FormData object and store to variable formData
           
                })
            })
                