<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/register', function () {
    return view('pages.register');
});

Route::get('/home', function () {
    return view('pages.home');
});

Route::get('/classes', function () {
    return view('pages.classes');
});

Route::get('/gallery', function () {
    return view('pages.gallery');
});

Route::get('/contact', function () {
    return view('pages.contact');
});

//user routes

Route::get('/user/profile', function () {
    return view('user.profile');
});

Route::get('/user/bookinghistory', function () {
    return view('user.bookinghistory');
});


//admin routes
Route::get('/admin/bookings', function () {
    return view('admin.bookings');
});
Route::get('/admin/classlist', function () {
    return view('admin.classlist');
});
Route::get('/admin/users', function () {
    return view('admin.users');
});

