// include the express framework
const express = require('express');

// instantiate an express app
const app = express();

//use passport for local auth
const passport = require('passport')

//include jwt for handling JSON web tokens
const jwt = require('jsonwebtoken')

// use custom strategies
require('./passport')

//include cors package to allow cross-origin requests
const cors = require('cors')

//allow cross-origin requests
app.use(cors())

// add body parsing to access the data in our request
const bodyParser = require('body-parser');

//include mongoose for handling multiple db queries/connections
const mongoose = require('mongoose');

//establish a connection to our database
const databaseUrl = 'mongodb+srv://CeciliaCortez:UFBatvJxBUVlomEY@cluster0-db3vs.mongodb.net/nanaycare_booking?retryWrites=true&w=majority';

//establish a connection to the remote db using the connect method of mongoose
mongoose.connect(databaseUrl, {useNewUrlParser : true});

mongoose.connection.once('open', () => {
	console.log('Remote database established');
})


// configure express/middleware to use the body parser
//package in retrieving request bodies
app.use(bodyParser.json());


const booking = require('./routes/booking.js');
app.use('/booking',booking);

const klase = require('./routes/klase.js');
app.use('/klase',klase);

const user = require('./routes/user.js');
app.use('/user',user);

const auth = require('./routes/auth.js');
app.use('/auth',auth);

app.use( (err, req, res, next) => {
	res.status(422).send({error : err.message })
})

const port = 3000;

app.listen(port, () => {
	console.log(`Ang proyekto ay nakahain sa ${port}`)
})