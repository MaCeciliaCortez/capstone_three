//load mongoose
const mongoose = require('mongoose')

//;oad mongoose schema for models
const Schema = mongoose.Schema

// create a new Schema for users
const UserSchema = new Schema({
	email : String,
	password : String,
	admin : {
		type : Boolean,
		default : false
	}
})


//set up the user model and export it to the main app
module.exports = mongoose.model('User', UserSchema)