//load mongoose library for data modelling
// import mongoose
const mongoose = require('mongoose');

// load mongoose schema for models
const Schema = mongoose.Schema;

//create a new Schema for Devs
const ClassSchema = new Schema({
	// name : String,
	name: {
		type : String,
		required : [true, 'name field is required']
	},
	// class : String,
	description: {
		type : String,
		required : [true, 'description field is required']
	}
})

//setup the Dev Model and export it to the main app
module.exports = mongoose.model('Class', ClassSchema);