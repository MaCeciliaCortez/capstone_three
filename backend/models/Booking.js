//load mongoose library for data modelling
// import mongoose
const mongoose = require('mongoose');

// load mongoose schema for models
const Schema = mongoose.Schema;

//create a new Schema for Devs
const BookingSchema = new Schema({
	// name : String,
	nameOfChild: {
		type : String,
		required : [true, 'Name field is required']
	},
	// class : String,
	class: {
		type : String,
		required : [true, 'Class field is required']
	},
	
	startDate : {
		type : Date,
		required : [true, 'Date field is required']
	},

	endDate: {
		type : Date,
		required : [true, 'Date field is required']
    },
    date: {
        type: Date,
        default: Date.now
    }
});

//setup the Dev Model and export it to the main app
module.exports = mongoose.model('Booking', BookingSchema);