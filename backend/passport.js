// passport configuration file

// use passport for local auth
const passport = require('passport')

//load passportJWT for auth of json web tokens
const passportJWT = require('passport-jwt')

//load JWT extraction for extracting web tokens later on
const ExtractJWT = passportJWT.ExtractJwt

// use local strategy for auth
const LocalStrategy = require('passport-local').Strategy

//use JWT strategy for auth users
const JWTStrategy = passportJWT.Strategy

// include the User Model
const UserModel = require('./models/User')

//include bcrypt for encrypting and decrypting
const bcrypt = require('bcrypt-nodejs')

//import mmoment for date parsing and handling
const moment = require('moment')


// configure passport.js architecture to use the local strategy
passport.use( new LocalStrategy({ usernameField : 'email' }, (email, password, done) => {
	//query for a user that matches the email provided
	UserModel.findOne({'email' : email})

	.then( (user) => {
		//uncomment this to voew the user being checked
		// console.log(user)

		if(!user) {
			// invalidate the user
			return done(null, false, {message : 'Invalid credentials.'})
		}

		if(email == user.email) {
			//check for the password hash compared to the stored password
			if(!bcrypt.compareSync(password, user.password)) {
				return done(null, false, {message : 'Invalid credentials.'})
			}

			// return successful login since a match is found
			return done(null, user)
		}

		//invalidate users
		return done(null, false, {message : 'invalid credentials'})
	})
}))



// configure passport for JWT usage in verification
passport.use(new JWTStrategy({
	jwtFromRequest : ExtractJWT.fromAuthHeaderAsBearerToken(),
	secretOrKey : 'secret-cq'
},
function(jwtPayload, cb) {
	// find the user in the db as needed
	// console.log(jwtPayload)
	return UserModel.findOne(jwtPayload.id)
	.then(user => {
		return cb(null, user)
	})
	.catch(err => {
		return cb(err)
	})
}
))













