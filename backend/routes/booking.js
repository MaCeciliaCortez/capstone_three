// include the express framework
const express = require('express');

// include the Router used in the express framework
const router = express.Router();

//require the dev model 
const BookingModel = require('../models/Booking');

// Add a new availability
router.post('/create', (req, res) => {

	// Build the new availability
	let newBooking = BookingModel({
		'nameOfChild': req.body.nameOfChild,
		'class': req.body.class,
		'startDate' : req.body.startDate,
		'endDate': req.body.endDate,
	});

  	// Proceed to save the information to the data base
	newBooking.save( (err, booking) => {

		// If there is a problem with the database, throw an error
		if(err){
			return res.status(500).json({
				'error': err
			});
		}
		// If database operation is successful return success
		return res.status(200).json({
			'data': {
				'message': 'booking added successfully',
				'booking': booking
			}
		});
	});
});

// Retrieve availabilities based on their id
router.get('/:id', (req, res) => {

	// Search for the specific availability based on the ID
	BookingModel.findOne({ '_id': req.params.id }).then( (booking) => {

		// If no threads are available, return a successful request with info
		if(!booking){
			return res.status(200).json({ 
				'message': 'No available booking to show.'
			});
		}

		// Return the satisfying availability based on the ID
		return res.status(200).json({
				'booking': booking
		});

	});

});

// Retrieve availability for updating
router.put('/:id', (req, res) => {

	BookingModel.updateOne( { '_id' : req.params.id}, req.body )
		.then( booking => {
			if(booking) {
				// let booking = BookingModel.findOne(req.params.id)
				// console.log(booking)
				return res.json({
					'data' : {
						'booking' : booking,
						'message' : 'Booking successfully updated.'
					}
				});
			}

			return res.json({
				'message' : 'No booking availability with the given ID found'
			});
		});
});

//delete an availability via its ID
router.delete('/:id', (req,res) => {
	BookingModel.deleteOne({'_id' : req.params.id})
		.then(booking => {
			if(booking) {
				return res.json({
					'data' : {
						'booking' : booking
					}
				});
			}

			return res.json({
				'message' : 'No availability with the given ID found'
			});
		});
});



module.exports = router;
