// include the express framework
const express = require('express');

// include the Router used in the express framework
const router = express.Router();

//require the dev model 
const KlaseModel = require('../models/Klase');

// Add a new availability
router.post('/create', (req, res) => {

	// Build the new availability
	let newKlase = KlaseModel({
		'name': req.body.name,
		'description': req.body.description,
	});

  	// Proceed to save the information to the data base
	newKlase.save( (err, klase) => {

		// If there is a problem with the database, throw an error
		if(err){
			return res.status(500).json({
				'error': err
			});
		}
		// If database operation is successful return success
		return res.status(200).json({
			'data': {
				'message': 'klase added successfully',
				'klase': klase
			}
		});
	});
});

// Retrieve availabilities based on their id
router.get('/:id', (req, res) => {

	// Search for the specific availability based on the ID
	KlaseModel.findOne({ '_id': req.params.id }).then( (klase) => {

		// If no threads are available, return a successful request with info
		if(!klase){
			return res.status(200).json({ 
				'message': 'No available class to show.'
			});
		}

		// Return the satisfying availability based on the ID
		return res.status(200).json({
				'klase': klase
		});

	});

});

// Retrieve availability for updating
router.put('/:id', (req, res) => {

	KlaseModel.updateOne( { '_id' : req.params.id}, req.body )
		.then( klase => {
			if(klase) {
				// let booking = BookingModel.findOne(req.params.id)
				// console.log(booking)
				return res.json({
					'data' : {
						'klase' : klase,
						'message' : 'Klase successfully updated.'
					}
				});
			}

			return res.json({
				'message' : 'No klase availability with the given ID found'
			});
		});
});

//delete an availability via its ID
router.delete('/:id', (req,res) => {
	KlaseModel.deleteOne({'_id' : req.params.id})
		.then(klase => {
			if(klase) {
				return res.json({
					'data' : {
						'klase' : klase
					}
				});
			}

			return res.json({
				'message' : 'No klase with the given ID found'
			});
		});
});



module.exports = router;
