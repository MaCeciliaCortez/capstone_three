// include the express framework
const express = require('express');

// include the Router used in the express framework
const router = express.Router();

//require the dev model 
const UserModel = require('../models/User');

const bcrypt = require('bcrypt-nodejs')

// Add a new availability
router.post('/create', (req, res) => {

	// Build the new availability
	let newUser = UserModel({
		'email': req.body.email,
		'password': req.body.pasword,
	});

  	// Proceed to save the information to the data base
	newUser.save( (err, user) => {

		// If there is a problem with the database, throw an error
		if(err){
			return res.status(500).json({
				'error': err
			});
		}
		// If database operation is successful return success
		return res.status(200).json({
			'data': {
				'message': 'user added successfully',
				'user': user
			}
		});
	});
});

// Retrieve availabilities based on their id
router.get('/:id', (req, res) => {

	// Search for the specific availability based on the ID
	UserModel.findOne({ '_id': req.params.id }).then( (user) => {

		// If no threads are available, return a successful request with info
		if(!user){
			return res.status(200).json({ 
				'message': 'No available user to show.'
			});
		}

		// Return the satisfying availability based on the ID
		return res.status(200).json({
				'user': user
		});

	});

});

// Retrieve availability for updating
router.put('/:id', (req, res) => {

	UserModel.updateOne( { '_id' : req.params.id}, req.body )
		.then( user => {
			if(user) {
				// let booking = BookingModel.findOne(req.params.id)
				// console.log(booking)
				return res.json({
					'data' : {
						'user' : user,
						'message' : 'User successfully updated.'
					}
				});
			}

			return res.json({
				'message' : 'No user availability with the given ID found'
			});
		});
});


//delete an availability via its ID
router.delete('/:id', (req,res) => {
	UserModel.deleteOne({'_id' : req.params.id})
		.then(user => {
			if(user) {
				return res.json({
					'data' : {
						'user' : user
					}
				});
			}

			return res.json({
				'message' : 'No user with the given ID found'
			});
		});
});

//register

//registration endpoint
router.post('/register', (req, res) => {
	// console.log(req.body)
	//create a variable called email and assign the value
	// of the email prop of our request body
	let email = req.body.email
	//create a variable called password and assign the value
	// of the password prop of our request body
	let password  = req.body.password
	// console.log(email)
	// console.log(password)
	if(!email || !password) {
		return res.status(500).json({
			'error' : 'incomplete'
		})
	}

	UserModel.find({'email' : email})
	.then( (users, err) => {
		// console.log(users)

		if(err) {
			return res.status(500).json({
				'error' : 'an error occured'
			})
		}

		//lets check if the email is already existing in the db
		if(users.length > 0) {
			return res.status(500).json({
				'error' : 'unggoy! it already exists'
			})
		}

		//lets use bcrypt to hash the password
		bcrypt.genSalt(10, function(err, salt) {
			// console.log(salt)
			// password = unhashed password
			// salt = salted string with the number of rounds of salting applied
			bcrypt.hash(password, salt, null, function(err, hash) {
				let newUser = UserModel({
					'email' : req.body.email,
					'password' : hash
				});

				// console.log(newUser)
				newUser.save(err => {
					if(!err) {
						return res.json({
							'message' : 'User Registered Successfully'
						})
					}
				})
			})

		})

	})




})

module.exports = router;
